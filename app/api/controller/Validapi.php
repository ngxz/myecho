<?php
namespace app\api\controller;

class Validapi{

	public $code = 1;

	public $msg = '操作成功';

	public $status = 'success';

    public function __construct(){
		$this->params = input('param.');
		$this->type = input('param.format') ? input('param.format') : '';
	}

    public function rt($result){
    	if(!$result && ($result === false)){
    		$this->code = 0;
    		$this->msg = $this->error;
    		$this->status = 'fail';
    		$this->data = '';
    	}
    	$data = array();
    	$data['code'] = $this->code;
    	$data['msg'] = $this->msg;
    	$data['status'] = $this->status;
    	$data['result'] = $result;
        ob_get_clean();
    	return $data;
    }

    public function getError() {
       return $this->error;
    }
}
