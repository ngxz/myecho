<?php
namespace app\user\model;

class User{
	public function __construct(){
		$this->userDb = getdb('admin');
	}

	public function lists($params){
		if (!$this->verify($params)) return false;

		$sqlmap = $this->parseSql($params);
		$field = $this->getField();
		$page = parsePage($params);
		$pagesize = parsePagesize($params);
		$order = paramsOrder($params);
		$result = $this->userDb->field($field)->where($sqlmap)->limit($pagesize)->page($page)->order($order)->select();
		if (empty($result)) {
			$this->error = '没有查到数据';
			return false;
		}
		
		return $result;
	}

	/**
	 * @return [type]
	 */
	public function getInfoByToken($params){
		if (!isset($params['token']) || empty($params['token'])) {
			$this->error = 'token错误';
			return false;
		}
		$sql = [];
		$sql['token'] = $params['token'];
		$result = $this->userDb->field($this->getField())->where($sql)->find();
		if(!$result){
			$this->error = '数据不存在';
			return false;
		}
		return $result;
	}

	/**
	 * [verify 验证输入参数]
	 * @return [type] [description]
	 */
	public function verify($params){
		if (false) {
			$this->error = '参数错误';
			return false;
		}
		return true;
	}

	/**
	 * [parseSql 组装SQL]
	 * @return [type] [description]
	 */
	public function parseSql($params){
		$sqlmap = array();
		if (!empty($params['status'])) {
			$sqlmap['status'] = $params['status'];
		}

		return $sqlmap;
	}

	/**
	 * [getField 获取某些字段]
	 * @return [type] [description]
	 */
	public function getField(){
		$field = array();
		$field['username'] = 'username';
		$field['id'] = 'id';
		$field['name'] = 'name';
		$field['login_num'] = 'login_num';
		$field['last_login_time'] = 'last_login_time';
		$field['timeout'] = 'timeout';
		return $field;
	}

	public function getError(){
		return $this->error;
	}

}