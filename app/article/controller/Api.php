<?php
namespace app\article\controller;

use app\api\controller\Validapi;
class Api extends Validapi{

	public function __construct(){
        parent::__construct();
    }

    public function lists(){
		$model = new \app\article\model\Article();
        $result = $model->lists($this->params);
        if (!$result) {
        	$this->error = $model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function detail(){
        $model = new \app\article\model\Article();
        $result = $model->findByid($this->params);
        if (!$result) {
            $this->error = $model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function tagLists(){
        $model = new \app\article\model\Articletag();
        $result = $model->lists($this->params);
        if (!$result) {
            $this->error = $model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function typeLists(){
        $model = new \app\article\model\Articletype();
        $result = $model->lists($this->params);
        if (!$result) {
            $this->error = $model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

}
