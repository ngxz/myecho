<?php
namespace app\article\controller;

use app\admin\controller\Validadmin;
class Admin extends Validadmin{

	public function __construct(){
        parent::__construct();
		$this->model = new \app\article\model\Article();
        $this->tagModel = new \app\article\model\Articletag();
        $this->typeModel = new \app\article\model\Articletype();
	}

    public function lists(){
        $result = $this->model->lists($this->params);
        if (!$result) {
        	$this->error = $this->model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function findByid(){
        $result = $this->model->findByid($this->params);
        if (!$result) {
            $this->error = $this->model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function add(){
        $result = $this->model->add($this->params);
        if (!$result) {
            $this->error = $this->model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function update(){
        $result = $this->model->update($this->params);
        if (!$result) {
            $this->error = $this->model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function delete(){
        $result = $this->model->delete($this->params);
        if (!$result) {
            $this->error = $this->model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function tagLists(){
        $result = $this->tagModel->lists($this->params);
        if (!$result) {
            $this->error = $this->tagModel->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function tagAdd(){
        $result = $this->tagModel->add($this->params);
        if (!$result) {
            $this->error = $this->tagModel->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function tagByid(){
        $result = $this->tagModel->tagByid($this->params);
        if (!$result) {
            $this->error = $this->tagModel->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function tagUpdate(){
        $result = $this->tagModel->update($this->params);
        if (!$result) {
            $this->error = $this->tagModel->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function tagDelete(){
        $result = $this->tagModel->delete($this->params);
        if (!$result) {
            $this->error = $this->tagModel->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function typeLists(){
        $result = $this->typeModel->lists($this->params);
        if (!$result) {
            $this->error = $this->typeModel->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function typeByid(){
        $result = $this->typeModel->typeByid($this->params);
        if (!$result) {
            $this->error = $this->typeModel->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function typeAdd(){
        $result = $this->typeModel->add($this->params);
        if (!$result) {
            $this->error = $this->typeModel->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function typeUpdate(){
        $result = $this->typeModel->update($this->params);
        if (!$result) {
            $this->error = $this->typeModel->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function typeDelete(){
        $result = $this->typeModel->delete($this->params);
        if (!$result) {
            $this->error = $this->typeModel->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

}
