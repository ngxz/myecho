<?php
namespace app\article\model;

class Article{
	public function __construct(){
		$this->articleDb = getdb('article');
	}

	/**
	 * [lists 条件查询列表]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public function lists($params){
		if (!$this->listsVerify($params)) return false;

		$sqlmap = $this->parseSql($params);
		$field = $this->getField();
		$page = parsePage($params);
		$pagesize = parsePagesize($params);
		$order = paramsOrder($params);
		$result = $this->articleDb->field($field)->where($sqlmap)->limit($pagesize)->page($page)->order($order)->select();
		if (empty($result)) {
			$this->error = '没有查到数据';
			return false;
		}
		$total = (int)$this->articleDb->where($sqlmap)->count();

		foreach ($result as $k => &$v) {
			if (empty($v['summary'])) {
				$v['summary'] = mb_substr($v['content'],0,80,'utf-8').'...';
			}
		}
		
		$data = array();
		$data['result'] = $result;
		$data['total'] = $total;
		return $data;
	}

	/**
	 * [findByid 通过id查找文章]
	 * @param  [type] $params [description]
	 * @param  string $fields [description]
	 * @return [type]         [description]
	 */
	public function findByid($params,$fields = '*'){
		if (!isset($params['id']) || empty($params['id'])) {
			$this->error = '文章id错误';
			return false;
		}
		$result = $this->articleDb->field($fields)->where('id',$params['id'])->find();
		if(!$result){
			$this->error = '该文章不存在';
			return false;
		}
		if (isset($result['category_id']) && !empty($result['category_id'])) {
			$mod_articletype = new \app\article\model\Articletype();
			$category_info = $mod_articletype->typeByid(['id'=>$result['category_id']],'id,name');
			if (!empty($category_info)) {
				$result['category_name'] = $category_info['name'];
			}
		}

		return $result;
	}

	/**
	 * [add 新增文章]
	 * @param [type] $params [description]
	 */
	public function add($params){
		if(!$this->verify($params)) return false;

		// 验证相同标题是否存在
		$_sqlmap = array();
		$_sqlmap['name'] = $params['name'];
		$row = $this->articleDb->where($_sqlmap)->find();
		if ($row) {
			$this->error = '新增失败,标题已存在';
			return false;
		}

		$data = array();
		if($params['name']){
			$data['name'] = $params['name'];	
		}
		if(isset($params['author'])){
			$data['author'] = $params['author'];
		}
		if(isset($params['content'])){
			$data['content'] = $params['content'];
		}
		if(isset($params['cover'])){
			$data['cover'] = $params['cover'];
		}
		if(isset($params['category_id'])){
			$data['category_id'] = $params['category_id'];
		}
		if(isset($params['is_recommend'])){
			$data['is_recommend'] = $params['is_recommend'];
		}
		if(isset($params['parise_num'])){
			$data['parise_num'] = $params['parise_num'];
		}
		if(isset($params['read_num'])){
			$data['read_num'] = $params['read_num'];
		}
		if(isset($params['sort'])){
			$data['sort'] = $params['sort'];
		}
		if(isset($params['status'])){
			$data['status'] = $params['status'];
		}
		$data['inserttime'] = _date();

		// add
		$result = $this->articleDb->insert($data);
		if(!$result){
			$this->error = '新增失败';
			return false;
		}
		return true;
	}
	
	/**
	 * [deleteByid 通过ID删除文章]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public function delete($params){
		if(!isset($params['ids']) || empty($params['ids'])){
			$this->error = 'id错误';
			return false;
		}
		$ids = $params['ids'];
		if(strpos($ids,',')){
			$ids = explode(',', $ids);
		}else{
			$ids = $ids;
		}
		$sqlmap = array();
		$sqlmap['id'] = array('IN',$ids);
		$data = array();
		$data['status'] = 0;
		$data['deltime'] = _date();
		$result = $this->articleDb->where($sqlmap)->update($data);
		if(!$result){
			$this->error = '删除失败';
			return false;
		}
		// 删除标签关联表和文章关联的
		$this->tagReDb->where(array('article_id'=>array('IN',$ids)))->delete();
		return true;
	}

	/**
	 * [update 更新文章]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public function update($params){
		if(!$this->verify($params)) return false;
		// 检查修改后的标题是否存在
		$_sqlmap = array();
		$_sqlmap['name'] = $params['name'];
		$_sqlmap['id'] = array('neq',$params['id']);
		$row = $this->articleDb->where($_sqlmap)->find();
		if ($row) {
			$this->error = '修改失败,标题已存在';
			return false;
		}

		$data = array();
		if($params['name']){
			$data['name'] = $params['name'];	
		}
		if($params['author']){
			$data['author'] = $params['author'];
		}
		if($params['summary']){
			$data['summary'] = $params['summary'];	
		}
		// 文章标签关联表插入数据，如果有的话
		if(!empty($params['tag_ids'])){
			// 删除该文章相关的标签关联
			$this->tagReDb->where(array('article_id'=>$params['id']))->delete();
			// 添加新的关联
			$this->addTagRelation($params['tag_ids'],$params['id']);
		}
		if($params['content']){
			$data['content'] = $params['content'];
		}
		if($params['cover']){
			$data['cover'] = $params['cover'];
		}
		if($params['category_id']){
			$data['category_id'] = $params['category_id'];
		}
		if(isset($params['is_recommend'])){
			$data['is_recommend'] = $params['is_recommend'];
		}
		if($params['parise_num']){
			$data['parise_num'] = $params['parise_num'];
		}
		if($params['read_num']){
			$data['read_num'] = $params['read_num'];
		}
		if($params['sort']){
			$data['sort'] = $params['sort'];
		}
		if(isset($params['status'])){
			$data['status'] = $params['status'];
		}
		$data['updatetime'] = _date();

		$result = $this->articleDb->where(array('id'=>$params['id']))->update($data);
		if(!$result){
			$this->error = '修改失败';
			return false;
		}
		return true;
	}

	/**
	 * [addTagRelation 文章标签关联表插入数据]
	 */
	public function addTagRelation($tagIds,$articleId){
		if(!empty($tagIds)){
			$_data = array();
			$_data['article_id'] = $articleId;
			$_data['inserttime'] = _date();

			if(strpos($tagIds,',')){
				$tags = explode(',',$tagIds);
				foreach ($tags as $k => $v) {
					$_data['tag_id'] = $v;
					$this->tagReDb->insert($_data);
				}
			}else{
				$_data['tag_id'] = $tagIds;
				$this->tagReDb->insert($_data);
			}
		}
	}

	/**
	 * [parseSql 组装SQL]
	 * @param  [type] $sqlmap [description]
	 * @return [type]         [description]
	 */
	private function parseSql($sqlmap){
		$_sqlmap = array();
		if(isset($sqlmap['name']) && !empty($sqlmap['name'])){
			$_sqlmap['name'] = array('LIKE','%'.$sqlmap['name'].'%');
		}
		if(isset($sqlmap['category_id']) && !empty($sqlmap['category_id'])){
			$_sqlmap['category_id'] = $sqlmap['category_id'];
		}
		if(isset($sqlmap['is_recommend']) && !empty($sqlmap['is_recommend'])){
			$_sqlmap['is_recommend'] = $sqlmap['is_recommend'];
		}
		if(isset($sqlmap['status']) && !empty($sqlmap['status'])){
			$_sqlmap['status'] = $sqlmap['status'];
		}
		if(isset($sqlmap['tags']) && !empty($sqlmap['tags'])){
			$_sqlmap['name'] = array('LIKE','%'.$sqlmap['tags'].'%');
		}
		if(isset($sqlmap['article_ids']) && !empty($sqlmap['article_ids'])){
			$_sqlmap['id'] = array('IN',$sqlmap['article_ids']);
			if(empty($sqlmap['article_ids'])){
				$_sqlmap['id'] = -1;
			}
		}
		return $_sqlmap;
	}

	private function verify($params){
		if(!$params['name']){
			$this->error = '文章名称错误';
			return false;
		}
		if(!$params['content']){
			$this->error = '文章内容错误！';
			return false;
		}
		if(!$params['category_id']){
			$this->error = '文章分类错误！';
			return false;
		}
		return true;
	}

	/**
	 * [setCover 根据文章类别，自动设置封面]
	 * @param [type] $category_id [description]
	 */
	private function setCover($category_id){
		switch ($category_id) {
			case 1:
				// 随笔
				return '/Public/static/img/thumb.jpg';
				break;
			case 2:
				// 前端
				return '/Public/static/img/web_default.jpg';
				break;
			case 3:
				// php
				return '/Public/static/img/php_default.jpg';
				break;
		}
	}

	public function addPriseNum($id){
		if(empty($id)){
			$this->error = 'id错误';
			return false;
		}
		$result = $this->model->where(array('id'=>$id))->setInc('parise_num',1);
		if(!$result){
			$this->error = $this->model->getError();
			return false;
		}
		$row = $this->model->field('parise_num')->where(array('id'=>$id))->find();
		return $row['parise_num'];
	}

	/**
	 * [listsVerify 验证输入参数]
	 * @return [type] [description]
	 */
	public function listsVerify($params){
		if (false) {
			$this->error = '参数错误';
			return false;
		}
		return true;
	}

	/**
	 * [getField 获取某些字段]
	 * @return [type] [description]
	 */
	public function getField(){
		$field = array();

		return $field;
	}

	public function getError(){
		return $this->error;
	}

}