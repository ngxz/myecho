<?php
namespace app\article\model;

class Articletype{
	public function __construct(){
		$this->typeDb = getdb('article_category'); 
	}

	/**
	 * [lists 条件查询列表]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public function lists($params){
		if (!$this->listsVerify($params)) return false;

		$sqlmap = $this->parseSql($params);
		$field = $this->getField();
		$page = parsePage($params);
		$pagesize = parsePagesize($params);
		$order = paramsOrder($params);
		$result = $this->typeDb->field($field)->where($sqlmap)->limit($pagesize)->page($page)->order($order)->select();
		if (empty($result)) {
			$this->error = '没有查到数据';
			return false;
		}

		$total = (int)$this->typeDb->where($sqlmap)->count();
		
		$data = array();
		$data['result'] = $result;
		$data['total'] = $total;
		return $data;
	}

	/**
	 * [typeByid 通过id查找文章分类]
	 * @param  [type] $params [description]
	 * @param  string $fields [description]
	 * @return [type]         [description]
	 */
	public function typeByid($params,$fields = '*'){
		if (!isset($params['id']) || empty($params['id'])) {
			$this->error = '分类id错误';
			return false;
		}
		$result = $this->typeDb->field($fields)->where('id',$params['id'])->find();
		if(!$result){
			$this->error = '文章分类不存在';
			return false;
		}
		return $result;
	}

	public function add($params){
		if(!$this->verify($params)) return false;

		// 检查名字是否存在
		$row = $this->typeDb->where(array('name'=>$params['name']))->find();
		if ($row) {
			$this->error = '添加失败，分类已存在';
			return false;
		}

		$params['sort'] = $params['sort'] ? $params['sort'] : 99;
		$params['status'] = $params['status'] ? $params['status'] : 1;
		$params['inserttime'] = _date();
		$result = $this->typeDb->insert($params);
		if(!$result){
			$this->error = '添加失败';
			return false;
		}
		return true;
	}

	public function delete($params){
		if(!isset($params['ids']) || empty($params['ids'])){
			$this->error = 'id错误';
			return false;
		}
		$ids = $params['ids'];
		if(strpos($ids,',')){
			$ids = explode(',', $ids);
		}else{
			$ids = $ids;
		}
		$sqlmap = array();
		$sqlmap['id'] = array('IN',$ids);
		$data = array();
		$data['status'] = 0;
		$data['deltime'] = _date();
		$result = $this->typeDb->where($sqlmap)->update($data);
		if(!$result){
			$this->error = '删除失败';
			return false;
		}
		return true;
	}

	public function update($params){
		if(!$this->verify($params)) return false;

		// 检查名字是否存在
		$_sql = array();
		$_sql['name'] = $params['name'];
		$_sql['id'] = array('neq',$params['id']);
		$row = $this->typeDb->where($_sql)->find();
		if ($row) {
			$this->error = '修改失败，分类名称已存在';
			return false;
		}

		$data = array();
		if($params['name']){
			$data['name'] = $params['name'];	
		}
		if($params['url']){
			$data['url'] = $params['url'];	
		}
		if($params['introduce']){
			$data['introduce'] = $params['introduce'];	
		}
		if($params['sort']){
			$data['sort'] = $params['sort'];
		}
		if(isset($params['status'])){
			$data['status'] = $params['status'];
		}
		$data['updatetime'] = _date();
		$result = $this->typeDb->where(array('id'=>$params['id']))->update($data);
		if(!$result){
			$this->error = '修改失败';
			return false;
		}
		return true;
	}

	/**
	 * [parseSql 组装SQL]
	 * @param  [type] $sqlmap [description]
	 * @return [type]         [description]
	 */
	private function parseSql($sqlmap){
		$_sqlmap = array();
		if(isset($sqlmap['name']) && !empty($sqlmap['name'])){
			$_sqlmap['name'] = array('LIKE','%'.$sqlmap['name'].'%');
		}
		if(isset($sqlmap['status']) && !empty($sqlmap['status'])){
			$_sqlmap['status'] = $sqlmap['status'];
		}
		return $_sqlmap;
	}

	private function verify($params){
		if(!$params['name']){
			$this->error = '分类名称错误';
			return false;
		}
		return true;
	}

	/**
	 * [listsVerify 验证输入参数]
	 * @return [type] [description]
	 */
	public function listsVerify($params){
		if (false) {
			$this->error = '参数错误';
			return false;
		}
		return true;
	}

	/**
	 * [getField 获取某些字段]
	 * @return [type] [description]
	 */
	public function getField(){
		$field = array();

		return $field;
	}

	public function getError(){
		return $this->error;
	}

}