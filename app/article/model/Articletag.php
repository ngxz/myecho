<?php
namespace app\article\model;

class Articletag{
	public function __construct(){
		$this->tagDb = getdb('article_tag');
		$this->tagReDb = getdb('tag_relationship'); 
	}

	/**
	 * [lists 条件查询列表]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public function lists($params){
		if (!$this->listsVerify($params)) return false;

		$sqlmap = $this->parseSql($params);
		$field = $this->getField();
		$page = parsePage($params);
		$pagesize = parsePagesize($params);
		$order = paramsOrder($params);
		$result = $this->tagDb->field($field)->where($sqlmap)->limit($pagesize)->page($page)->order($order)->select();
		if (empty($result)) {
			$this->error = '没有查到数据';
			return false;
		}

		$total = (int)$this->tagDb->where($sqlmap)->count();
		
		$data = array();
		$data['result'] = $result;
		$data['total'] = $total;
		return $data;
	}

	/**
	 * [tagByid 通过id查找TAG]
	 * @param  [type] $params [description]
	 * @return [type]     [description]
	 */
	public function tagByid($params){
		if (!isset($params['id']) || empty($params['id'])) {
			$this->error = '标签id错误';
			return false;
		}
		$result = $this->tagDb->where('id',$params['id'])->find();
		if(!$result){
			$this->error = '文章标签不存在';
			return false;
		}
		return $result;
	}

	public function add($params){
		if(!$this->verify($params)) return false;

		// 检查名字是否存在
		$row = $this->tagDb->where(array('name'=>$params['name']))->find();
		if ($row) {
			$this->error = '添加失败，标签名已存在';
			return false;
		}

		$params['sort'] = $params['sort'] ? $params['sort'] : 99;
		$params['status'] = $params['status'] ? $params['status'] : 1;
		$params['inserttime'] = _date();
		$result = $this->tagDb->insert($params);
		if(!$result){
			$this->error = '添加失败';
			return false;
		}
		return true;
	}

	public function delete($params){
		if(!isset($params['ids']) || empty($params['ids'])){
			$this->error = 'id错误';
			return false;
		}
		$ids = $params['ids'];
		if(strpos($ids,',')){
			$ids = explode(',', $ids);
		}else{
			$ids = $ids;
		}
		$sqlmap = array();
		$sqlmap['id'] = array('IN',$ids);
		$data = array();
		$data['status'] = 0;
		$data['deltime'] = _date();
		$result = $this->tagDb->where($sqlmap)->update($data);
		if(!$result){
			$this->error = '删除失败';
			return false;
		}
		return true;
	}

	public function update($params){
		if(!$this->verify($params)) return false;

		// 检查名字是否存在
		$_sql = array();
		$_sql['name'] = $params['name'];
		$_sql['id'] = array('neq',$params['id']);
		$row = $this->tagDb->where($_sql)->find();
		if ($row) {
			$this->error = '更新失败，标签名已存在';
			return false;
		}

		$data = array();
		if($params['name']){
			$data['name'] = $params['name'];	
		}
		if($params['sort']){
			$data['sort'] = $params['sort'];
		}
		if(isset($params['status'])){
			$data['status'] = $params['status'];
		}
		$data['updatetime'] = _date();
		$result = $this->tagDb->where(array('id'=>$params['id']))->update($data);
		if(!$result){
			$this->error = '更新失败';
			return false;
		}
		return true;
	}

	/**
	 * 通过标签id来查询关联的文章id
	 */
	public function getArticleIdByid($id){
		$rows = $this->tagReDb->field('article_id')->where(array('tag_id'=>$id))->select();
		if(!$rows){
			$this->error = '没有相关数据';
			return false;
		}
		$result = array();
		foreach ($rows as $k => $v) {
			$result[$k] = $v['article_id'];
		}
		return $result;
	}

	/**
	 * [parseSql 组装SQL]
	 * @param  [type] $sqlmap [description]
	 * @return [type]         [description]
	 */
	private function parseSql($sqlmap){
		$_sqlmap = array();
		if(isset($sqlmap['name']) && !empty($sqlmap['name'])){
			$_sqlmap['name'] = array('LIKE','%'.$sqlmap['name'].'%');
		}
		if(isset($sqlmap['tags']) && !empty($sqlmap['tags'])){
			$_sqlmap['id'] = array('IN',$sqlmap['tags']);
		}
		if(isset($sqlmap['status']) && !empty($sqlmap['status'])){
			$_sqlmap['status'] = $sqlmap['status'];
		}
		return $_sqlmap;
	}

	private function verify($params){
		if(!$params['name']){
			$this->error = '标签名错误';
			return false;
		}
		return true;
	}

	/**
	 * [listsVerify 验证输入参数]
	 * @return [type] [description]
	 */
	public function listsVerify($params){
		if (false) {
			$this->error = '参数错误';
			return false;
		}
		return true;
	}

	/**
	 * [getField 获取某些字段]
	 * @return [type] [description]
	 */
	public function getField(){
		$field = array();

		return $field;
	}

	public function getError(){
		return $this->error;
	}

}