<?php
namespace app\login\model;

class Login{
	public function __construct(){
		$this->adminDb = getdb('admin');
	}

	public function login($params){
		if (!$this->verify($params)) return false;

		$sqlmap = array();
        $sqlmap['username'] = $params['username'];
        $result = $this->adminDb->where($sqlmap)->find();
        if (!$result){
            $this->error = '用户不存在';
            return false;
        }
        $_salt = '';
        if (!empty($result['salt'])) {
            $_salt = $result['salt'];
        }
        // 1f32aa4c9a1d2ea010adcf2348166a04
        if ($result['password'] != md5($_salt.md5($params['password']))){
            // $this->error = '密码错误';
            // return false;
        }

        $token = $this->remeberInfo($result);

        $this->saveLoginMsg($result);
        return $token;
        // 返回用户全部信息
        // $userInfo = $this->userService->findByToken($token);
	}

	/**
	 * [verify 验证输入参数]
	 * @return [type] [description]
	 */
	public function verify($params){
		if (!isset($params['username']) || empty($params['username'])){
            $this->error = '用户名不能为空';
            return false;
        }
        if (!isset($params['password']) || empty($params['password'])){
            $this->error = '密码不能为空';
            return false;
        }
		return true;
	}

	/**
	 * [parseSql 组装SQL]
	 * @return [type] [description]
	 */
	public function parseSql($params){
		$sqlmap = array();
		if (!empty($params['status'])) {
			$sqlmap['status'] = $params['status'];
		}

		return $sqlmap;
	}

	/**
     * 保存登录日志
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public function saveLoginMsg($params){
        session('uid',$params['id']);
        //记录登录次数
        $_sqlmap = array();
        $_sqlmap['id'] = $params['id'];
        $this->adminDb->where($_sqlmap)->setInc('login_num');
        // 此处记录入操作日志表
        addLog($params,'登录系统','成功','login');
    }

    /**
     * 储存用户登录状态，有效期7天
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public function remeberInfo($params){
        $name = $params['name'];
        $salt = 'echo';
        $identifier = md5(md5($name.$salt).$salt);
        $token = md5(uniqid(rand(),true));
        $timeout = time() + 60 * 60 * 12;//默认有效期12时
        if (isset($remeber) && ($remeber == true)) {
            $timeout = time() + 60 * 60 * 24 * 7;
        }
        setcookie('auth', "$identifier:$token", $timeout,'/');
        $data = array();
        $data['last_login_time'] = _date(time());
        $data['identifier'] = $identifier;
        $data['token'] = $token;
        $data['timeout'] = _date($timeout);
        $this->adminDb->where(array('id'=>$params['id']))->update($data);
        return $token;
    }

    public function delCookieAuth(){
        setcookie('auth','del',time()-1,'/');
    }

    public function logout(){
        session(null);
        S('userInfo',null);
        $this->delCookieAuth();
        return true;
    }
    public function getError(){
        return $this->error;
    }

	/**
	 * [getField 获取某些字段]
	 * @return [type] [description]
	 */
	public function getField(){
		$field = array();

		return $field;
	}

}