<?php
namespace app\login\controller;

use app\api\controller\Validapi;
class Api extends Validapi{

	public function __construct(){
        parent::__construct();
		$this->model = new \app\login\model\Login();
	}

    public function login(){
        $result = $this->model->login($this->params);
        if (!$result) {
            $this->error = $this->model->getError();
            addLog($this->params,'登录系统',$this->error,'login');
        }
        $data = $this->rt($result);
        return json($data);
    }
}
