<?php
namespace app\user\controller;

use app\api\controller\Validapi;
class Api extends Validapi{

	public function __construct(){
        parent::__construct();
		$this->model = new \app\user\model\Log();
	}

    public function lists(){
        $result = $this->model->lists($this->params);
        if (!$result) {
        	$this->error = $this->model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }
}
