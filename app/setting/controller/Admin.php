<?php
namespace app\setting\controller;

use app\admin\controller\Validadmin;
class Admin extends Validadmin{

	public function __construct(){
        parent::__construct();
		$this->model = new \app\setting\model\Setting();
	}

    public function find(){
        $result = $this->model->find();
        if (!$result) {
        	$this->error = $this->model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function add(){
        $result = $this->model->add($this->params);
        if (!$result) {
            $this->error = $this->model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function update(){
        $result = $this->model->update($this->params);
        if (!$result) {
            $this->error = $this->model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function delete(){
        $result = $this->model->delete($this->params);
        if (!$result) {
            $this->error = $this->model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }
}
