<?php
namespace app\setting\controller;

use app\api\controller\Validapi;
class Api extends Validapi{

	public function __construct(){
        parent::__construct();
		$this->model = new \app\setting\model\Setting();
	}

    public function find(){
        $result = $this->model->find();
        if (!$result) {
        	$this->error = $this->model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }
}
