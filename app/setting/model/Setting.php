<?php
namespace app\setting\model;

class Setting{
	public function __construct(){
		$this->db = getdb('setting');
	}

	public function find(){
		$result = $this->db->select();
		if(!$result){
			$this->error = '查询失败';
			return false;
		}
		$data = array();
		foreach($result as $k => $v){
			$data[$v['key']] = $v['value'];
		}
		return $data;
	}

	public function update($params){
		if (isset($params['token'])) {
			unset($params['token']);
		}
		foreach ($params as $k => $v) {
			$sqlmap = array();
			$sqlmap['key'] = $k;
			$row = $this->db->where($sqlmap)->find();
			$data = array();
			$data['key'] = $k;
			$data['value'] = $v;
			$data['updatetime'] = _date();
			if(!$row){
				$this->db->insert($data);
				continue;
			}
			$result = $this->db->where($sqlmap)->update($data);
		}
		return true;
	}

	public function getError(){
		return $this->error;
	}

}