<?php
use \think\Db;
use \think\Debug;
use \think\Cache;
// 应用公共文件
/**
 * [getdb 数据库连接]
 * @param  string $table  [description]
 * @param  string $constr [description]
 * @return [type]         [description]
 */
function getdb($table =""){
    // $con = config($constr);
    return Db::connect()->name($table);
}

/**
 * [parsePage 获取页码]
 * @param  [type] $params [description]
 * @return [type]         [description]
 */
function parsePage($params){
	$page = 1;
	if (isset($params['page']) && !empty($params['page'])) {
		$page = $params['page'];
	}
	return $page;
}

/**
 * [parsePage 获取分页长度]
 * @param  [type] $params [description]
 * @return [type]         [description]
 */
function parsePagesize($params){
	$pagesize = 10;
	if (isset($params['pagesize']) && !empty($params['pagesize'])) {
		$pagesize = $params['pagesize'];
	}
	return $pagesize;
}

/**
 * [parsePage 获取排序]
 * @param  [type] $params [description]
 * @return [type]         [description]
 */
function paramsOrder($params){
	$order = 'inserttime desc';
	if (isset($params['order']) && !empty($params['order'])) {
		$order = $params['order'];
	}
	return $order;
}

/**
 * [_date 格式化日期]
 * @return [type] [description]
 */
function _date($date = "",$formate = 'Y-m-d H:i:s'){
	if (!$date) {
		$date = time();
	}
	return date($formate,$date); 
}

function addLog($params,$action = '',$note = '',$type = ''){
    $data = array();
    // 登陆时存用户名
    if ($type == 'login') {
        $data['username'] = isset($params['username']) ? $params['username'] : '';
        $data['userid'] = isset($params['id']) ? $params['id'] : '';
    }else{
        // 返回用户全部信息
        $userInfo = getdb('admin')->where(array('token'=>$params['token']))->find();
        $data['userid'] = $userInfo['id'];
        $data['username'] = $userInfo['name'];
    }
    $data['action'] = $action;
    $data['note'] = $note;
    if (!$note) {
        $data['note'] = '成功';
    }
    $data['inserttime'] = _date(time());
    getdb('log')->insert($data);
}

function getRedis(){
    try{
        $redisCache = Cache::store('redis');
        // $res = Cache::store('redis')->handler();
        if ($redisCache->get('asdweawea') === false) {
            return $redisCache;
        }
        // $redis = new redis();
        // if($redis->connect('127.0.0.1',6379)){
        //     return $redis;
        // }else{
        //     // throw new Exception('连接redis服务器失败');
        //     return false;
        // }
    }catch(Exception $e){
        // echo $e->getMessage();
        return false;
    }
}