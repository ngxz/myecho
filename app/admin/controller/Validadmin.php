<?php
namespace app\admin\controller;
class Validadmin{

    public $code = 0;

    public $msg = '操作成功';

    public $status = 'success';

    public function __construct(){
        // $authStatus = $this->checkAuth(input('param.'));
        // if (!$authStatus) {
        //     echo $this->error;
        //     exit;
        // }
        $params = input('param.');
        if (isset($params['token'])) {
            unset($params['token']);
        }
        if (isset($params['limit'])) {
            $params['pagesize'] = $params['limit'];
        }
        $this->params = $params;
        $this->type = input('param.format') ? input('param.format') : '';
	}

    public function rt($result){
    	if(!$result){
    		$this->code = 1;
    		$this->msg = $this->error;
    		$this->status = 'fail';
    		$this->data = '';
    	}
    	$data = array();
    	$data['code'] = $this->code;
    	$data['msg'] = $this->msg;
    	$data['status'] = $this->status;
        $data['data'] = $result;
        if (isset($result['result'])) {
           $data['data'] = $result['result'];
        }
        if (isset($result['total'])) {
            $data['count'] = $result['total'];
        }
    	return $data;
    }

    /**
     * 检查token是否过期
     * @return [type] [description]
     */
    public function checkAuth($params){
        return true;
        if (!isset($params['token']) || empty($params['token'])) {
            $this->error = '请重新登陆';
            return false;
        }
        $token = $params['token'];
        $usermodel = new \app\user\model\User();
        $result = $usermodel->getInfoByToken(array('token'=>$token));
        if(empty($result)){
            $this->error = '身份信息错误';
            return false;
        }
        if (strtotime($result['timeout']) < time()) {
            $this->error = '身份有效期已过';
            return false;
        }
        return true;
    }

    public function getError() {
       return $this->error;
    }
}
