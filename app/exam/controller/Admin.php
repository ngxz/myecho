<?php
namespace app\exam\controller;

use app\admin\controller\Validadmin;
class Admin extends Validadmin{

	public function __construct(){
        parent::__construct();
	}

    public function lists(){
        $model = new \app\exam\model\Exam();
        $result = $model->lists($this->params);
        if (!$result) {
            $this->error = $model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function add(){
        $model = new \app\exam\model\Exam();
        $result = $model->add($this->params);
        if (!$result) {
            $this->error = $model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function update(){
        $model = new \app\exam\model\Exam();
        $result = $model->update($this->params);
        if (!$result) {
            $this->error = $model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function detail(){
        $model = new \app\exam\model\Exam();
        $result = $model->findByid($this->params);
        if (!$result) {
            $this->error = $model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function examTypeLists(){
        $model = new \app\exam\model\ExamType();
        $result = $model->getAllExamType();
        if (!$result) {
            $this->error = $model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function examSubjectLists(){
        $model = new \app\exam\model\ExamSubject();
        $result = $model->getAllExamSubject();
        if (!$result) {
            $this->error = $model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function examTagLists(){
        $model = new \app\exam\model\ExamTag();
        $result = $model->getAllExamTag();
        if (!$result) {
            $this->error = $model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function examAllTagType(){
        $model = new \app\exam\model\ExamTag();
        $result = $model->getAllTagType();
        if (!$result) {
            $this->error = $model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function tagNameById(){
        $model = new \app\exam\model\ExamTag();
        $id = isset($this->params['id']) ? $this->params['id'] :'';
        $result = $model->getTagNameById($id);
        if (!$result) {
            $this->error = $model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

}
