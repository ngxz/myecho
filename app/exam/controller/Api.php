<?php
namespace app\exam\controller;

use app\api\controller\Validapi;
class Api extends Validapi{

	public function __construct(){
        parent::__construct();
    }

    public function lists(){
		$model = new \app\exam\model\Exam();
        $result = $model->lists($this->params);
        if (!$result) {
        	$this->error = $model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function detail(){
        $model = new \app\exam\model\Exam();
        $result = $model->findByid($this->params);
        if (!$result) {
            $this->error = $model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function examTypeLists(){
        $model = new \app\exam\model\ExamType();
        $result = $model->getAllExamType();
        if (!$result) {
            $this->error = $model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function examSubjectLists(){
        $model = new \app\exam\model\ExamSubject();
        $result = $model->getAllExamSubject();
        if (!$result) {
            $this->error = $model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

}
