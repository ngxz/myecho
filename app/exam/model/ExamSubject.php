<?php
namespace app\exam\model;
/**
 * 科目表
 */
class ExamSubject{

	public $examSubject = array(
		// ['code'	=>	'03708','name'	=>	'中国近代史纲要'],
		// ['code'	=>	'03709','name'	=>	'马克思主义基本原理概论'],
		// ['code'	=>	'00015','name'	=>	'英语（二）'],
		// ['code'	=>	'00023','name'	=>	'高等数学（工本）'],
		['code'	=>	'02326','name'	=>	'操作系统'],
		['code'	=>	'04741','name'	=>	'计算机网络原理'],
		['code'	=>	'02325','name'	=>	'计算机系统结构'],
		['code'	=>	'02197','name'	=>	'概率论与数理统计（二）'],
		['code'	=>	'02324','name'	=>	'离散数学'],
		['code'	=>	'04747','name'	=>	'Java语言程序设计（一）'],
		['code'	=>	'02331','name'	=>	'数据结构'],
		['code'	=>	'04735','name'	=>	'数据库系统原理'],
		['code'	=>	'04737','name'	=>	'C++程序设计'],
		['code'	=>	'02333','name'	=>	'软件工程'],
		['code'	=>	'07999','name'	=>	'毕业设计']
	);
	
	public function __construct(){
	}

	public function getAllExamSubject(){
		return $this->examSubject;
	}

	public function getError(){
		return $this->error;
	}

}