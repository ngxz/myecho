<?php
namespace app\exam\model;
/**
 * 题型分类表
 */
class ExamType{
	
	public $examType = array(
		['id' => 1,'name' => '单选题'],
		['id' => 2,'name' => '填空题'],
		['id' => 3,'name' => '简答题'],
		['id' =>4,'name' => '计算题'],
		['id' =>5,'name' =>	'综合应用题'],
		['id' =>6,'name' =>	'证明题']
		// ['id' =>7,'name' =>	'多选题'],
		// ['id' =>8,'name' =>	'其他题']
	);

	public $examTypeSubjectId = array(
		'1'=>array('name'=>'单选题','subject_id'=>1),
		'2'=>array('name'=>'单选题','subject_id'=>2),
		'3'=>array('name'=>'单选题','subject_id'=>3),
		'4'=>array('name'=>'单选题','subject_id'=>4),
		'5'=>array('name'=>'单选题','subject_id'=>5),
		'6'=>array('name'=>'单选题','subject_id'=>6),
		'7'=>array('name'=>'单选题','subject_id'=>7),
		'8'=>array('name'=>'单选题','subject_id'=>8),
		'9'=>array('name'=>'单选题','subject_id'=>9),
		'10'=>array('name'=>'单选题','subject_id'=>10),
		'11'=>array('name'=>'单选题','subject_id'=>11),
		'12'=>array('name'=>'单选题','subject_id'=>12),
		'13'=>array('name'=>'单选题','subject_id'=>13),
		'14'=>array('name'=>'单选题','subject_id'=>14),

		'15'=>array('name'=>'单选题','subject_id'=>1),
		'16'=>array('name'=>'单选题','subject_id'=>2),
		'17'=>array('name'=>'单选题','subject_id'=>3),
		'18'=>array('name'=>'单选题','subject_id'=>4),
		'19'=>array('name'=>'单选题','subject_id'=>5),
		'20'=>array('name'=>'单选题','subject_id'=>6),
		'21'=>array('name'=>'单选题','subject_id'=>7),
		'22'=>array('name'=>'单选题','subject_id'=>8),
		'23'=>array('name'=>'单选题','subject_id'=>9),
		'24'=>array('name'=>'单选题','subject_id'=>10),
		'25'=>array('name'=>'单选题','subject_id'=>11),
		'26'=>array('name'=>'单选题','subject_id'=>12),
		'27'=>array('name'=>'单选题','subject_id'=>13),
		'28'=>array('name'=>'单选题','subject_id'=>14)
	);

	public function __construct(){
	}

	public function getAllExamType(){
		return $this->examType;
	}

	public function getTypeNameById($id = ''){
		if (!$id) {
			$this->error = 'id不能为空';
			return false;
		}
		$examtype_data = $this->examType;
		$examtype_name = '';
		foreach ($examtype_data as $v) {
			if ($v['id'] == $id) {
				$examtype_name = $v['name'];
				break;
			}
		}
		return $examtype_name;
	}

	public function getError(){
		return $this->error;
	}

}