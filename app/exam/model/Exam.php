<?php
namespace app\exam\model;

class Exam{
	public function __construct(){
		$this->cache_prefix = 'cache_exam_';
		$this->examDb = getdb('exam');
	}

	/**
	 * [lists 条件查询列表]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public function lists($params){
		if (!$this->listsVerify($params)) return false;

		$sqlmap = $this->parseSql($params);
		$field = $this->getField();
		$page = parsePage($params);
		$pagesize = parsePagesize($params);
		// 按插入时间倒序，真实试卷题型
		$params['order'] = 'type_id asc,inserttime desc';
		$order = paramsOrder($params);
		$result = $this->examDb->field($field)->where($sqlmap)->limit($pagesize)->page($page)->order($order)->select();
		if (empty($result)) {
			$this->error = '没有查到数据';
			return false;
		}

		$mod_examtag = new \app\exam\model\ExamTag();
		$mod_examtype = new \app\exam\model\ExamType();
		foreach ($result as $k => &$v) {
			$tag_name_arr = [];

			// 加入题型
			if (isset($v['type_id']) && !empty($v['type_id'])) {
				$type_name = $mod_examtype->getTypeNameById($v['type_id']);
				if ($type_name) {
					$tag_name_arr[] = $type_name;
				}
			}
			// 标签
			if (isset($v['tag_id']) && !empty($v['tag_id'])) {
				$tag_id_arr = explode(',', $v['tag_id']);
				foreach ($tag_id_arr as $key => $val) {
					// 获取类型
					$tag_name = $mod_examtag->getTagNameById(substr($val, 0,1));
					if ($tag_name) {
						$tag_name_arr[] = $tag_name;
					}
					// 获取类型详情
					if (strlen($val) > 1) {
						$tag_name = $mod_examtag->getTagYearById($val);
						if (!empty($tag_name)) {
							$tag_name_arr[] = $tag_name['year'].$tag_name['month'];
						}
					}
				}
			}
			$v['tag_str'] = array_unique($tag_name_arr);
		}

		// 缓存获取
		$redisCache = getRedis();
		// var_dump($redisCache);die;

		if ($redisCache) {
			$tmp_cache_prefix = $this->cache_prefix.'list_total_num';
			$cache_total = $redisCache->get($tmp_cache_prefix);
			if ($cache_total) {
				$total = $cache_total;
			}else{
				$total = (int)$this->examDb->where($sqlmap)->count();
				$redisCache->set($tmp_cache_prefix,$total,60*60*24);
			}
		}else{
			$total = (int)$this->examDb->where($sqlmap)->count();
		}
		
		$data = array();
		$data['result'] = $result;
		$data['total'] = $total;
		return $data;
	}

	/**
	 * [findByid 通过id查找题目]
	 * @param  [type] $params [description]
	 * @param  string $fields [description]
	 * @return [type]         [description]
	 */
	public function findByid($params,$fields = '*'){
		if (!isset($params['id']) || empty($params['id'])) {
			$this->error = '题目id错误';
			return false;
		}
		$result = $this->examDb->field($fields)->where('id',$params['id'])->find();
		if(!$result){
			$this->error = '该题目不存在';
			return false;
		}
		$mod_examtag = new \app\exam\model\ExamTag();
		$tag_name_arr = [];
		// 加入题型
		if (isset($result['type_id']) && !empty($result['type_id'])) {
			$mod_examtype = new \app\exam\model\ExamType();
			$type_name = $mod_examtype->getTypeNameById($result['type_id']);
			if ($type_name) {
				$tag_name_arr[] = $type_name;
			}
		}
		// 标签
		if (isset($result['tag_id']) && !empty($result['tag_id'])) {
			$tag_id_arr = explode(',', $result['tag_id']);
			foreach ($tag_id_arr as $key => $val) {
				// 获取类型
				$tag_name = $mod_examtag->getTagNameById(substr($val, 0,1));
				if ($tag_name) {
					$tag_name_arr[] = $tag_name;
				}
				// 获取类型详情
				if (strlen($val) > 1) {
					$tag_name = $mod_examtag->getTagYearById($val);
					if (!empty($tag_name)) {
						$tag_name_arr[] = $tag_name['year'].$tag_name['month'];
					}
				}
			}
		}
		$result['tag_str'] = array_unique($tag_name_arr);
		return $result;
	}

	/**
	 * [add 新增题目]
	 * @param [type] $params [description]
	 */
	public function add($params){
		if(!$this->verify($params)) return false;

		// 验证相同标题是否存在
		$_sqlmap = array();
		$_sqlmap['name'] = $params['name'];
		$row = $this->examDb->where($_sqlmap)->find();
		if ($row) {
			$this->error = '新增失败,标题已存在';
			return false;
		}

		$data = array();
		if(!isset($params['spec_id'])){
			// 默认为计算机专业
			$data['spec_id'] = 1;
		}
		// 标题
		if($params['name']){
			$data['name'] = $params['name'];	
		}
		// 科目
		if($params['subject_id']){
			$data['subject_id'] = $params['subject_id'];
		}
		// 题型
		if($params['type_id']){
			$data['type_id'] = $params['type_id'];
		}
		// 1开头的是真题，需要区小类分年月，保存tag_id
		// 非1开头的直接保存
		$tag_id_arr = '';
		$tag_id_arr[] = $params['tag_id'];
		$tag_id_arr[] = $params['tagtype_id'];
		$data['tag_id'] = implode(',', $tag_id_arr);
		// 题干
		if($params['content']){
			$data['content'] = $params['content'];
		}
		// 答案
		if($params['answer']){
			$data['answer'] = $params['answer'];
		}
		// 解析
		if($params['explain']){
			$data['explain'] = $params['explain'];
		}
		if(isset($params['note'])){
			$data['note'] = $params['note'];
		}
		$data['inserttime'] = _date();

		// add
		$result = $this->examDb->insert($data);
		if(!$result){
			$this->error = '新增失败';
			return false;
		}
		return $result;
	}
	
	/**
	 * [deleteByid 通过ID删除题目]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public function delete($params){
		if(!isset($params['ids']) || empty($params['ids'])){
			$this->error = 'id错误';
			return false;
		}
		$ids = $params['ids'];
		if(strpos($ids,',')){
			$ids = explode(',', $ids);
		}else{
			$ids = $ids;
		}
		$sqlmap = array();
		$sqlmap['id'] = array('IN',$ids);
		$data = array();
		$data['status'] = 0;
		$data['deltime'] = _date();
		$result = $this->examDb->where($sqlmap)->update($data);
		if(!$result){
			$this->error = '删除失败';
			return false;
		}
		// 删除标签关联表和题目关联的
		$this->tagReDb->where(array('exam_id'=>array('IN',$ids)))->delete();
		return true;
	}

	/**
	 * [update 更新题目]
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public function update($params){
		// if(!$this->verify($params)) return false;
		// 检查修改后的标题是否存在
		$_sqlmap = array();
		$_sqlmap['name'] = $params['name'];
		$_sqlmap['id'] = array('neq',$params['id']);
		$row = $this->examDb->where($_sqlmap)->find();
		if ($row) {
			$this->error = '修改失败,标题已存在';
			return false;
		}

		$data = array();
		if($params['name']){
			$data['name'] = $params['name'];	
		}
		if($params['content']){
			$data['content'] = $params['content'];
		}
		if($params['answer']){
			$data['answer'] = $params['answer'];
		}
		if($params['explain']){
			$data['explain'] = $params['explain'];
		}
		$data['updatetime'] = _date();

		$result = $this->examDb->where(array('id'=>$params['id']))->update($data);
		if(!$result){
			$this->error = '修改失败';
			return false;
		}
		return true;
	}

	/**
	 * [addTagRelation 题目标签关联表插入数据]
	 */
	public function addTagRelation($tagIds,$examId){
		if(!empty($tagIds)){
			$_data = array();
			$_data['exam_id'] = $examId;
			$_data['inserttime'] = _date();

			if(strpos($tagIds,',')){
				$tags = explode(',',$tagIds);
				foreach ($tags as $k => $v) {
					$_data['tag_id'] = $v;
					$this->tagReDb->insert($_data);
				}
			}else{
				$_data['tag_id'] = $tagIds;
				$this->tagReDb->insert($_data);
			}
		}
	}

	/**
	 * [parseSql 组装SQL]
	 * @param  [type] $sqlmap [description]
	 * @return [type]         [description]
	 */
	private function parseSql($sqlmap){
		$_sqlmap = array();
		if(isset($sqlmap['searchword']) && !empty($sqlmap['searchword'])){
			$_sqlmap['name'] = array('LIKE','%'.$sqlmap['searchword'].'%');
		}
		if(isset($sqlmap['subject_id']) && !empty($sqlmap['subject_id'])){
			$_sqlmap['subject_id'] = $sqlmap['subject_id'];
		}
		// if(isset($sqlmap['tags']) && !empty($sqlmap['tags'])){
		// 	$_sqlmap['name'] = array('LIKE','%'.$sqlmap['tags'].'%');
		// }
		if(isset($sqlmap['type_id']) && !empty($sqlmap['type_id'])){
			$_sqlmap['type_id'] = array('IN',$sqlmap['type_id']);
		}
		// 年月都有的时候
		if((isset($sqlmap['year']) && !empty($sqlmap['year'])) && (isset($sqlmap['month']) && !empty($sqlmap['month']))){
			$_sqlmap['tag_id'] = array();
			$_sqlmap['tag_id'][] = array('LIKE','1'.substr($sqlmap['year'], -2).'%');
			$_sqlmap['tag_id'][] = array('LIKE','1__'.$sqlmap['month'].'%');
		}else{
			if (isset($sqlmap['year']) && !empty($sqlmap['year'])) {
				$_sqlmap['tag_id'] = array('LIKE','1'.substr($sqlmap['year'], -2).'%');
			}
			if(isset($sqlmap['month']) && !empty($sqlmap['month'])){
				$_sqlmap['tag_id'] = array('LIKE','1__'.$sqlmap['month'].'%');
			}
		}
		$_sqlmap['stat'] = 1;
		return $_sqlmap;
	}

	private function verify($params){
		if(!$params['name']){
			$this->error = '题目名称错误';
			return false;
		}
		if(!$params['content']){
			$this->error = '题目内容错误！';
			return false;
		}
		if(!$params['subject_id']){
			$this->error = '试题科目错误！';
			return false;
		}
		if(!$params['type_id']){
			$this->error = '试题类别错误！';
			return false;
		}
		return true;
	}

	/**
	 * [listsVerify 验证输入参数]
	 * @return [type] [description]
	 */
	public function listsVerify($params){
		if (false) {
			$this->error = '参数错误';
			return false;
		}
		return true;
	}

	/**
	 * [getField 获取某些字段]
	 * @return [type] [description]
	 */
	public function getField(){
		$field = array();
		$field['id'] = 'id';
		$field['name'] = 'name';
		$field['tag_id'] = 'tag_id';
		$field['type_id'] = 'type_id';
		return $field;
	}

	public function getError(){
		return $this->error;
	}

}