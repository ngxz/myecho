<?php
namespace app\exam\model;
/**
 * 标签表
 */
class ExamTag{

	public $examTag = array(
		'12010' => ['year'	=>	'2020','month'	=>	'10'],
		'12008' => ['year'	=>	'2020','month'	=>	'08'],
		'11910' => ['year'	=>	'2019','month'	=>	'10'],
		'11904' => ['year'	=>	'2019','month'	=>	'04'],
		'11810' => ['year'	=>	'2018','month'	=>	'10'],
		'11804' => ['year'	=>	'2018','month'	=>	'04'],
		'11810' => ['year'	=>	'2018','month'	=>	'10'],
		'11804' => ['year'	=>	'2018','month'	=>	'04'],
		'11710' => ['year'	=>	'2017','month'	=>	'10'],
		'11704' => ['year'	=>	'2017','month'	=>	'04'],
		'11610' => ['year'	=>	'2016','month'	=>	'10'],
		'11604' => ['year'	=>	'2016','month'	=>	'04']
	);

	public $tagType = array(
		'1' => '真题',
		'2' => '重点',
		'3' => '难点'
	);

	public $error = '';
	
	public function __construct(){
	}

	public function getAllExamTag(){
		return $this->examTag;
	}

	public function getAllTagType(){
		return $this->tagType;
	}

	public function getTagNameById($id = ''){
		if (!$id) {
			$this->error = 'id不能为空';
			return false;
		}
		return $this->tagType[$id];
	}

	public function getTagYearById($id = ''){
		if (!$id) {
			$this->error = 'id不能为空';
			return false;
		}
		return $this->examTag[$id];
	}

	public function getError(){
		return $this->error;
	}

}