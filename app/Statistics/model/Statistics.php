<?php
namespace app\Statistics\model;

class Statistics{
	public function __construct(){
		$this->articleDb = getdb('article');
	}

	/**
     * 新增文章的月数据
     */
    public function addArticleMonth($params){
        $date = '';
        if (isset($params['date']) && !empty($params['date'])) {
            $date = $params['date'];
        }
        $sqlmap = array();
        if($date){
            $month_start = strtotime($date);//指定月份月初时间戳
            $month_end = mktime(23, 59, 59, date('m', strtotime($date))+1, 00);//指定月份月末时间戳
        }else{
            $month_start = mktime(0, 0 , 0,date("m"),1,date("Y"));
            $month_end = mktime(23,59,59,date("m"),date("t"),date("Y"));
        }
        $sqlmap['time'] = array(array('EGT',$month_start),array('ELT',$month_end));
        $result = $this->articleDb->where($sqlmap)->count();
        return (int)$result;
    }

    /**
     * 发布文章的年数据
     * @param string $date [年份，默认为今年]
     */
    public function addArticleYear($params){
        $date = '';
        if (isset($params['date']) && !empty($params['date'])) {
            $date = $params['date'];
        }
        $year = _date(time(),'Y');
        $result = array();
        if($date){
            $year = $date;
        }
        for ($i=1; $i <=12 ; $i++) {
            $_date = $year.'-'.$i;
            $month_start = strtotime($_date);//指定月份月初时间戳
            $month_end = mktime(23, 59, 59, date('m', strtotime($_date))+1, 00);//指定月份月末时间戳
            $sqlmap = array();
            $sqlmap['time'] = array(array('EGT',$month_start),array('ELT',$month_end));
            $result[] = $this->articleDb->where($sqlmap)->count();
        }
        return $result;
    }

    /**
     * @param  文章阅读数最多的
     * @return [type]
     */
    public function readArticleTop($params){
        $limit = 5;
        if (isset($params['limit']) && !empty($params['limit'])) {
            $limit = $params['limit'];
        }
        $order = 'read_num DESC';
        if (isset($params['order']) && !empty($params['order'])) {
            $order = $params['order'];
        }
        $result = $this->articleDb->field(array('id','name','read_num'))->limit($limit)->order($order)->select();
        return $result;
    }

    /**
     * @param  文章阅读数最多的
     * @return [type]
     */
    public function pariseArticleTop($params){
        $limit = 5;
        if (isset($params['limit']) && !empty($params['limit'])) {
            $limit = $params['limit'];
        }
        $order = 'read_num DESC';
        if (isset($params['order']) && !empty($params['order'])) {
            $order = $params['order'];
        }
        $result = $this->articleDb->field(array('id','name','parise_num'))->limit($limit)->order($order)->select();
        return $result;
    }

	/**
	 * [parseSql 组装SQL]
	 * @return [type] [description]
	 */
	public function parseSql($params){
		$sqlmap = array();
		if (!empty($params['status'])) {
			$sqlmap['status'] = $params['status'];
		}

		return $sqlmap;
	}

    public function getError(){
        return $this->error;
    }

	/**
	 * [getField 获取某些字段]
	 * @return [type] [description]
	 */
	public function getField(){
		$field = array();

		return $field;
	}

}