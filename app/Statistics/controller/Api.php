<?php
namespace app\Statistics\controller;

use app\api\controller\Validapi;
class Api extends Validapi{

	public function __construct(){
        parent::__construct();
		$this->model = new \app\Statistics\model\Statistics();
	}

    public function addArticleMonth(){
        $result = $this->model->addArticleMonth($this->params);
        if (!$result && ($result === false)) {
            $this->error = $this->model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function addArticleYear(){
        $result = $this->model->addArticleYear($this->params);
        if (!$result && ($result === false)) {
            $this->error = $this->model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function readArticleTop(){
        $result = $this->model->readArticleTop($this->params);
        if (!$result && ($result === false)) {
            $this->error = $this->model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

    public function pariseArticleTop(){
        $result = $this->model->pariseArticleTop($this->params);
        if (!$result && ($result === false)) {
            $this->error = $this->model->getError();
        }
        $data = $this->rt($result);
        return json($data);
    }

}
