# myecho

#### 介绍
发布文章的单独模块，前端使用数据和后端管理数据通过接口


#### 使用说明

请求规则

前端数据地址：/模块/Api/接口

后台管理数据地址：/模块/Admin/接口

模块：需要请求的模块名，如文章模块，Article

接口：需要请求的接口名，如列表，Lists

返回参数类型：默认json格式

返回参数

参数名	类型	说明

code	int	返回状态码（1：成功，0：失败）

msg	string	返回描述内容

status	string	返回状态描述（success:成功，fail：失败）

result	string	成功后返回的数据

返回示例


  {
    "code": 1,
    "msg": "操作成功",
    "status": "success",
    "result": ""
  }



